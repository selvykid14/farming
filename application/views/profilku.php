<div class="bradcam_area breadcam_bg_4">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="bradcam_text text-center">
                        <h3>Profil Investor</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ bradcam_area  -->

	<div class="container">
		<div class="row my-2">
			<div class="col-lg-8 order-lg-2">
				<ul class="nav nav-tabs">
					<li class="nav-item">
						<a href="" data-target="#profile" data-toggle="tab" class="nav-link active">ProfilKu</a>
					</li>
					<li class="nav-item">
						<a href="" data-target="#messages" data-toggle="tab" class="nav-link">Pemberitahuan</a>
					</li>
					<li class="nav-item">
						<a href="" data-target="#edit" data-toggle="tab" class="nav-link">Edit Profil</a>
					</li>
				</ul>
				<div class="tab-content py-4">
					<div class="tab-pane active" id="profile">
						<h3 class="mb-3">PROFIL INVESTOR</h3>
						<div class="row">
						
							<div class="col-md-12">
								<h5 class="mt-2"><span class="float-right"></span> Nama Lengkap     : <?php echo $data->NAMA_MEMBER; ?></h5>
							</div>

                            <div class="col-md-12">
								<h5 class="mt-2"><span class="float-right"></span> Nomor KTP        : <?php echo $data->NO_KTP_MEMBER; ?></h5>
                            </div>

                            <div class="col-md-12">
								<h5 class="mt-2"><span class="float-right"></span> Nomor Telepon    : <?php echo $data->NO_TELEPON_MEMBER; ?></h5>
                            </div>

                            <div class="col-md-12">
								<h5 class="mt-2"><span class="float-right"></span> Alamat             : <?php echo $data->ALAMAT_MEMBER; ?></h5>
                            </div>

                            <div class="col-md-12">
								<h5 class="mt-2"><span class="float-right"></span> Nomor Rekening     : <?php echo $data->NO_REKENING_MEMBER; ?></h5>
                            </div>

                            <div class="col-md-12">
								<h5 class="mt-2"><span class="float-right"></span> EMAIL               : <?php echo $data->EMAIL_MEMBER; ?></h5>
                            </div>

                            <div class="col-md-12">
								<h5 class="mt-2"><span class="float-right"></span> Username            : <?php echo $data->USERNAME_MEMBER; ?></h5>
                            </div>
						

						</div>
						<!--/row-->
					</div>
					<div class="tab-pane" id="messages">
						<div class="alert alert-info alert-dismissable">
							<a class="panel-close close" data-dismiss="alert">×</a> This is an <strong>.alert</strong>. Use this to show important messages to the user.
						</div>
						<table class="table table-hover table-striped">
							<tbody>                                    
								<tr>
									<td>
									   <span class="float-right font-weight-bold">3 hrs ago</span> Here is your a link to the latest summary report from the..
									</td>
								</tr>
								<tr>
									<td>
									   <span class="float-right font-weight-bold">Yesterday</span> There has been a request on your account since that was..
									</td>
								</tr>
								<tr>
									<td>
									   <span class="float-right font-weight-bold">9/10</span> Porttitor vitae ultrices quis, dapibus id dolor. Morbi venenatis lacinia rhoncus. 
									</td>
								</tr>
								<tr>
									<td>
									   <span class="float-right font-weight-bold">9/4</span> Vestibulum tincidunt ullamcorper eros eget luctus. 
									</td>
								</tr>
								<tr>
									<td>
									   <span class="float-right font-weight-bold">9/4</span> Maxamillion ais the fix for tibulum tincidunt ullamcorper eros. 
									</td>
								</tr>
							</tbody> 
						</table>
					</div>
					<div class="tab-pane" id="edit">
					
						<form action="<?php echo base_url(). 'Proses/simpan'; ?>" method="post" >
							<div class="form-group row">
							<input class="form-control" type="hidden" value="<?php echo $id = $this->session->userdata('id_member');?>" name="id">
								<label class="col-lg-3 col-form-label form-control-label">Nama Lengkap</label>
								<div class="col-lg-9">
									<input class="form-control" type="text" value="<?php echo $data->NAMA_MEMBER;?>" name="nama">
								</div>
                            </div>
                            
                            <div class="form-group row">
								<label class="col-lg-3 col-form-label form-control-label">Nomor KTP</label>
								<div class="col-lg-9">
									<input class="form-control" type="text" placeholder="31xxxxxxx" value="<?php echo $data->NO_KTP_MEMBER;?>" name="no_ktp">
								</div>
                            </div>
                            
                            <div class="form-group row">
								<label class="col-lg-3 col-form-label form-control-label">Nomor Telepon</label>
								<div class="col-lg-9">
									<input class="form-control" type="text" placeholder="0xxxxxxxxxx" value="<?php echo $data->NO_TELEPON_MEMBER;?>" name="no_telp">
								</div>
                            </div>

                            <div class="form-group row">
								<label class="col-lg-3 col-form-label form-control-label">Alamat</label>
								<div class="col-lg-9">
									<input class="form-control" type="text" value="<?php echo $data->ALAMAT_MEMBER;?>" name="alamat">
								</div>
                            </div>

                            <div class="form-group row">
								<label class="col-lg-3 col-form-label form-control-label">Nomor Rekening</label>
								<div class="col-lg-9">
									<input class="form-control" type="text" placeholder="0xxxxxxxxxx" value="<?php echo $data->NO_REKENING_MEMBER;?>" name="no_rek">
								</div>
							</div>
							
							<div class="form-group row">
								<label class="col-lg-3 col-form-label form-control-label">Email</label>
								<div class="col-lg-9">
									<input class="form-control" type="text" value="<?php echo $data->EMAIL_MEMBER;?>" name="email">
								</div>
                            </div>
                            
                            <div class="form-group row">
								<label class="col-lg-3 col-form-label form-control-label">Username</label>
								<div class="col-lg-9">
									<input class="form-control" type="text" value="<?php echo $data->USERNAME_MEMBER;?>" name="usernam">
								</div>
                            </div>

							<div class="form-group row">
								<label class="col-lg-3 col-form-label form-control-label">Foto Profil</label>
								<div class="col-lg-9">
									<input class="form-control" type="file" name="foto">
								</div>
                            </div>						

							
							<div class="form-group row">
								<label class="col-lg-3 col-form-label form-control-label"></label>
								<div class="col-lg-9">
									<input class="btn btn-primary" type="submit" value="Simpan Profil"></button>
								</div>
							</div>
							
						</form>
						
					</div>
				</div>
			</div>

			<br>
			
			<div class="col-lg-4 order-lg-1 text-center">
			
			<?php if(!$this->session->userdata('id_member')) {?>
				<img src="http://placehold.it/150" class="mx-auto img-fluid img-circle d-block" alt="Foto Profil">
				<h6 class="mt-2">Unggah foto profil</h6>
				<label class="custom-file">
					<input type="file" class="custom-file-input" id="FOTO_MEMBER" name="FOTO_MEMBER">
					<span class="custom-file-control">Pilih Foto</span>
				</label>
				<div class="mt-3">
                	<button type="submit" value="upload" class="btn btn-primary">Unggah</button>
                    	<?php echo form_close();?> 
                </div>
			<?php }else { ?>
				<img src="<?php echo base_url().$data->FOTO_MEMBER;?>" width="150" class="mx-auto img-fluid img-circle d-block" alt="Foto Profil">
			<?php }?>	
			
			</div>
			
		</div>
	</div>
</br>
