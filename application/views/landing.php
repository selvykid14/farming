<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>E-FARMING</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/img/favicon.png') ?>">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.carousel.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/magnific-popup.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/themify-icons.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/nice-select.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/flaticon.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/gijgo.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/animate.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/slick.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/slicknav.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>">
</head>

<body>
    <!--[if lte IE 9]>
        <![endif]-->

    <!-- header-start -->
    <header>
        <div class="header-area ">
            <div id="sticky-header" class="main-header-area" style="background-color:black;">
                <div class="container">
                    <div class="header_bottom_border">
                        <div class="row align-items-center">
                            <div class="col-xl-3 col-lg-2">
                                <div class="logo">
                                    <a href="<?php echo base_url();?>landing">
                                        <img src="<?php echo base_url('assets/img/logo.png') ?>" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-7">
                                <div class="main-menu  d-none d-lg-block">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a class="active" href="<?php echo base_url()?>landing">beranda</a></li>
                                            <li><a href="<?php echo base_url();?>landing/profil">Profil</a></li>
                                            <li><a href="<?php echo base_url();?>landing/kontak">Kontak</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 d-none d-lg-block">
                                <div class="Appointment">
                                    
                                    <div class="book_btn d-none d-lg-block">
                                        <a class="" href="<?php echo base_url()?>landing/login">Login</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </header>
<!--header end-->


    <!-- service_area_start -->
    <div class="service_area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title text-center mb-50">
                        <h3>Promo Investasi</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-md-4">
                    <div class="single_service service_bg_1">
                        <div class="service_hover">
							<a href="<?php echo base_url()?>home/deskripsi1">
								<img src="<?php echo base_url('assets/img/svg_icon/legal-paper.svg')?>" alt="">
								<h3>Promo investasi 1</h3>
								<div class="hover_content">
									<div class="hover_content_inner">
										<h4>Promo investasi 1</h4>
										<p>Proyek investasi ternak sapi yang berlokasi di Boyolali, Jawa Tengah</p>
									</div>
								</div>
							</a>
                        </div>
                    </div>
                    <div style="text-align: center">
                    <a class="btn btn-primary" type="submit" href="<?php echo base_url()?>home/deskripsi3">LIHAT DETAIL</a>
				    <a class="btn btn-success" type="submit" href="<?php echo base_url()?>home/login">INVESTASI</a>
                    </div>
                </div>
			
                <div class="col-xl-4 col-md-4">
                    <div class="single_service service_bg_2">
                        <div class="service_hover">
							<a href="<?php echo base_url()?>home/deskripsi2">                          
								<img src="<?php echo base_url('assets/img/svg_icon/case.svg')?>" alt="">
								<h3>Promo investasi 2</h3>
								<div class="hover_content">
									<div class="hover_content_inner">
										<h4>Promo investasi 2</h4>
                                        <p>Proyek investasi ternak kambing qurban yang berlokasi di Malang, Jawa Timur</p>
									</div>
								</div>
							</a>
                        </div>
                    </div>
                    <div style="text-align: center">
                    <a class="btn btn-primary" type="submit" href="<?php echo base_url()?>home/deskripsi3">LIHAT DETAIL</a>
				    <a class="btn btn-success" type="submit" href="<?php echo base_url()?>home/login">INVESTASI</a>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4">
                    <div class="single_service service_bg_3">
                        <div class="service_hover">
 							<a href="<?php echo base_url()?>home/deskripsi3">
								<img src="<?php echo base_url('assets/img/svg_icon/survey.svg')?>" alt="">
								<h3>Promo investasi 3</h3>
								<div class="hover_content">
										<div class="hover_content_inner">
											<h4>Promo investasi 3</h4>
											<p>Proyek investasi ternak sapi impor yang berlokasi di Kabupaten Semarang, Jawa Tenngah</p>
										</div>
								</div>
							</a>
                        </div>
                    </div>
                    <div style="text-align: center">
                    <a class="btn btn-primary" type="submit" href="<?php echo base_url()?>home/deskripsi3">LIHAT DETAIL</a>
				    <a class="btn btn-success" type="submit" href="<?php echo base_url()?>home/login">INVESTASI</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- service_area_end -->

    <!-- about  -->
    <div class="about_area ">
        <div class="container-fluid p-0">
            <div class="row no-gutters align-items-center">
                <div class="col-xl-6 col-lg-6">
                    <div class="about_image">
                        <img src="<?php echo base_url('assets/img/about/myfarm.jpg')?>" alt="">
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6">
                    <div class="about_info">
                        <h3>Kenapa harus E-FARMING ?</h3>
                        <ul>
                            <li> E-Farming membuka peluang investasi peternakan yang menjanjikan </li>
                            <li> Terbagi dalam beberapa slot dalam setiap investasinya </li>
                            <li> Mengeluarkan modal yang Minim </li>
							<li> Dapat melihat laporan harian </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ about  -->

<br>
	<div id="fh5co-about">
    <div class="container">
    <div class="row">
        <div class="h-100 row align-items-center">
            <h1>Macam - Macam Investasi E-Farming</h1>
        </div>
    <hr>
		<div class="container">
            <div class="row">
			        <div class="col-md-4 col-md-offset-2 text-justify heading-section animate-box" data-animate-effect="fadeIn">
                    <div class="fh5co-staff">
					<img class="img-responsive" src="<?php echo base_url('assets/img/kambing.png')?>">
						<h3>Investasi Kambing</h3>
						<p>Investasi ternak kambing yang terbagi dalam 5 slot tiap investasinya</p>
                    </div>	
                </div>

                    <div class="col-md-4 col-md-offset-2 text-justify heading-section animate-box">
                    <div class="fh5co-staff">
					<img class="img-responsive" src="<?php echo base_url('assets/img/sapi.png') ?>">
						<h3>Investasi Sapi</h3>
                        <p>Investasi hewan ternak yaitu sapi yang terbagi dalam 10 slot tiap investasi</p>
                    </div>
                </div>
					
                    <div class="col-md-4 col-md-offset-2 text-justify heading-section animate-box">
                    <div class="fh5co-staff">
					<img class="img-responsive" src="<?php echo base_url('assets/img/domba.png') ?>">
                        <h3>Investasi Domba</h3>
                        <p>Investasi hewan ternak yaitu domba yang terbagi dalam beberapa slot bagi investor</p>
                    </div>
                </div>

                <div class="col-md-4 col-md-offset-2 text-justify heading-section animate-box">
                    <div class="fh5co-staff">
					<img class="img-responsive" src="<?php echo base_url('assets/img/bebek.png') ?>">
                        <h3>Investasi Bebek</h3>
                        <p>Investasi hewan ternak berupa bebek yang terbagi dalam beberapa slot bagi investor</p>
                    </div>
                </div>

                <div class="col-md-4 col-md-offset-2 text-justify heading-section animate-box">
                    <div class="fh5co-staff">
					<img class="img-responsive" src="<?php echo base_url('assets/img/ayam.png') ?>">
                        <h3>Investasi Ayam</h3>
                        <p>Investasi hewan ternak berupa ayam yang terbagi dalam beberapa slot</p>
                    </div>
                </div>

            </div>
            </div>
            </div>
    </hr>
            </div>
        </div>
    </div>
</br>

    <!-- financial_solution 
    <div class="financial_solution_area">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6">
                    <div class="financial_active owl-carousel">
                        <div class="single_finalcial_wrap">
                            <h3>Investasi Kambing <br>
                                    </h3>
                                    <p>terbagi dalam 5 slot tiap investasi nya</p>
                        </div>
                        <div class="single_finalcial_wrap">
                            <h3>investasi Sapi<br>
                                    </h3>
                                    <p>Terbagi dalam 10 slot tiap investasi</p>
                        </div>
                        <div class="single_finalcial_wrap">
                            <h3>E-FARMING<br>
                                    </h3>
                                    <p>Investasi Mudah dengan Modal Minim yang Terpercaya</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6">
                    <div class="finance_thumb">
                        <img src="<?php echo base_url('assets/img/about/farming.jpg')?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>-->
    <!--/ financial_solution -->

    <!-- project  -->
    <div class="project_area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="project_info text-center">
                        <h3>Ada pertanyaan?</h3>
                        <p><br>
                                </p>
                        <a href="<?php echo base_url('home/kontak')?>" class="boxed-btn3-white">Hubungi kami</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ project  -->

    <!-- footer start -->
