
<hr>
<div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5 mx-auto">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row" >
          <div class="col-lg-2"></div>                  
          <div class="col-lg-8">
            <div class="p-5">
              <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Masuk Akun</h1>
                  </div>

                <?=  $this->session->flashdata('message')?>
                
                <?php echo form_open_multipart('proses/cek_login');?>
					      <form class="user" method="post" action="<?= base_url('home/login'); ?>">
                    
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" 
                      id="email" name="email" placeholder="Enter Email Address..." value="<?= set_value('email'); ?>">
                      <?= form_error('email','<small class="text-danger pl-3">','</small>'); ?>
                    </div>
                    
                      <div class="form-group">
                      <input type="password" class="form-control form-control-user"
                       id="password" name="password" placeholder="Password">
                       <?= form_error('email','<small class="text-danger pl-3">','</small>'); ?>
                    </div>
                    <button type="sumbit" class="btn btn-primary btn-user btn-block">
                      Login
                    </button>
                    <hr>
                  </form>  
                  
                  <?php echo form_close();?>
                  
                  <div class="text-center">
                    <a class="small" href="<?= base_url('landing/registrasi');?>">Belum Memiliki Akun? Silahkan Registrasi</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>
  </div>
  <hr>

