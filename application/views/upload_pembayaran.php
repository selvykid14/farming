<div class="portfolio_details_area">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2">
                    <div class="portfolio_details_content text-center mb-50">
                        <h3>Upload Bukti Pembayaran</h3>
                            <p>Upload Bukti Pembaran yang telah Anda lakukan. </p>
                    </div>
                    
                    <div class="container mt-3">
                    <?php echo form_open_multipart('proses/saveUpload');?>
                        <!--<form action="" enctype="multipart/form-data">-->

                          <p>Unggah Bukti Pembayaran :</p>
                          <div class="custom-file mb-3">
                            <input type="file" class="custom-file-input" id="BUKTI_PEMBAYARAN" name="BUKTI_PEMBAYARAN">
                            <label class="custom-file-label" for="customFile">Pilih file</label>
                          </div>

                <br>  
                      <div class="mt-3">
                            <button type="submit" value="upload" class="btn btn-primary">Submit</button>
                            <?php echo form_close();?> 
                          </div>
                        <!--</form>-->
                      </div>
                </br>
                      
                      <script>
                      // Add the following code if you want the name of the file appear on select
                      $(".custom-file-input").on("change", function() {
                        var fileName = $(this).val().split("\\").pop();
                        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                      });
                      </script>
                
                </div> 
            </div>
        </div>
    </div>

<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-3.2.1.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
 
 $('#submit').submit(function(e){
     e.preventDefault(); 
          $.ajax({
              url:'<?php echo base_url();?>home/upload_pembayaran/do_upload',
              type:"post",
              data:new FormData(this),
              processData:false,
              contentType:false,
              cache:false,
              async:false,
               success: function(data){
                   alert("Upload Image Berhasil.");
            }
          });
        });
</script>
