
    <!-- bradcam_area  -->
    <div class="bradcam_area bradcam_bg_1">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="bradcam_text text-center">
                        <h3>Profil</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ bradcam_area  -->


    <!-- about wrap  -->
    <div class="about_wrap_area about_wrap_area2">
		<div class="container">
		</div>
    </div>
    <!--/ about wrap  -->

<!-- about  -->
<div class="about_area">
    <div class="container-fluid p-0">
        <div class="row no-gutters align-items-center">
            <div class="col-xl-6 col-lg-6">
                <div class="about_image">
                    <img src="<?php echo base_url('assets/img/about/myfarm.jpg')?>" alt="">
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
                <div class="about_info">
                    <div class="col-md-8 col-md-offset-1 text-justify heading-section animate-box">
                    <h3>E FARMING</h3>
							<p>E-Farming Membuka Peluang kepada semua orang untuk dapat melakukan investasi secara mudah dengan layanan berbasis online dengan modal ringan. E-Farming merupakan Perusahaan yang berdiri sejak tahun 2020 memiliki komitmen yang tinggi tentang pemberdayaan kelompok mitra peternak. Banyak peternak masih menggunakan pola tradisional dalam pembudidayaan ternak sapi. Kondisi tersebut mengakibatkan peternak sering merugi dalam mengembangkan usahanya. Pada titik terendah, banyak peternak yang meninggalkan profesinya.</p>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ about  -->

    <!-- counter  -->
	<div class="about_wrap_area about_wrap_area2">
        <div class="section_title text-center ">
            <h3>Visi | Misi | Tujuan</h3>
        </div>
    <!--/ counter  -->

<!-- visi misi-->
    <div id="fh5co-contact">
				<div class="container">
				<div class="row">
                
                        <div class="col-md-4 col-md-offset-2 text-justify heading-section animate-box">
                            <h3>Visi </h3>
                                <p>Menjadi Tempat berinvestasi yang Terbaik</p>
                        </div>

                        <div class="col-md-4 col-md-offset-2 text-justify heading-section fh5co-heading">
                            <h3>Misi </h3>
                            <ul type="square">
							    <li>1. Memberikan tawaran investasi yang bermutu</li>
								<li>2. Memberikan penawaran investasi dengan modal minim</li>
								<li>3. Menghasilkan Rizki dari Peternakan</li>
                            </ul>
                        </div>
                        
                        <div class="col-md-4 col-md-offset-2 text-justify heading-section animate-box">
                            <h3>Tujuan </h3> 
                            <ul>
									<li>1. Mengamankan kekayaan dari inflasi melalui peternakan</li>
									<li>2. Menyediakan dana pensiun buat hari tua nanti</li>
									<li>3. Mendapatkan penghasilan tetap</li>
									<li>4. Menghasilkan tambahan tanpa ribet</li>
								    <li>5. Memberikan layanan edukasi dan komunikasi kepada masyarakat tentang halal</li>									
							</ul>
                        </div> 
                        </div> 
                </div>
            </div>
        </div>

<!-- selesai visi misi-->

    <!-- project  -->
    
    <!--/ project  -->

