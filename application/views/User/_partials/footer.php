
   
    <!-- footer start -->
    <footer class="footer">
        <div class="footer_top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-md-6 col-lg-4">
                        <div class="footer_widget">
                            <div class="footer_logo">
                                <a href="#">
                                    <img src="<?php echo base_url('assets/img/logo.png')?>" alt="">
                                </a>
                            </div>
                            <p>
                                Firmament morning sixth subdue darkness
                                creeping gathered divide.
                            </p>
                            <div class="socail_links">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <i class="ti-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="ti-twitter-alt"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-2 offset-xl-1 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                    Services
                            </h3>
                            <ul>
                                <li><a href="#">Design</a></li>
                                <li><a href="#">Development</a></li>
                                <li><a href="#">Marketing</a></li>
                                <li><a href="#">Consulting</a></li>
                                <li><a href="#">Finance</a></li>
                            </ul>

                        </div>
                    </div>
                    <div class="col-xl-2 col-md-6 col-lg-2">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                    Useful Links
                            </h3>
                            <ul>
                                <li><a href="#">About</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#"> Contact</a></li>
                                <li><a href="#"> Free quote</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Address
                            </h3>
                            <ul>
                                    <li>200, D-block, Green lane USA</li>
                                    <li>+10 367 467 8934</li>
                                    <li><a href="#"> docmed@contact.com</a></li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right_text">
            <div class="container">
                <div class="footer_border"></div>
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right text-center">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!--/ footer end  -->

    <!-- link that opens popup -->

    <!-- form itself end-->
    <form id="test-form" class="white-popup-block mfp-hide">
        <div class="popup_box ">
            <div class="popup_inner">
                <div class="popup_header">
                    <h3>Get Free Quote</h3>
                </div>
                <div class="custom_form">
                        <div class="row">
                                <div class="col-xl-12">
                                    <input type="text" placeholder="Your Name">
                                </div>
                                <div class="col-xl-12">
                                    <input type="email" placeholder="Email">
                                </div>
                                <div class="col-xl-12">
                                    <textarea name="" id="" cols="30" rows="10" placeholder="Message"></textarea>
                                </div>
                                <div class="col-xl-12">
                                    <button type="submit" class="boxed-btn3">Send</button>
                                </div>
                            </div>
                </div>
            </div>
        </div>
    </form>
    <!-- form itself end -->
    <!-- JS here -->
 <!-- JS here -->
    <script src="<?php echo base_url('assets/js/invest.js');?>"</script>
    <script src="<?php echo base_url('assets/js/vendor/modernizr-3.5.0.min.js');?>"</script>
    <script src="<?php echo base_url('assets/js/vendor/jquery-1.12.4.min.js');?>"</script>
    <script src="<?php echo base_url('assets/js/popper.min.js');?>"</script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"</script>
    <script src="<?php echo base_url('assets/js/owl.carousel.min.js');?>"</script>
    <script src="<?php echo base_url('assets/js/isotope.pkgd.min.js');?>"</script>
    <script src="<?php echo base_url('assets/js/ajax-form.js');?>"</script>
    <script src="<?php echo base_url('assets/js/waypoints.min.js');?>"</script>
    <script src="<?php echo base_url('assets/js/jquery.counterup.min.js');?>"</script>
    <script src="<?php echo base_url('assets/js/imagesloaded.pkgd.min.js');?>"</script>
    <script src="<?php echo base_url('assets/js/scrollIt.js');?>"</script>
    <script src="<?php echo base_url('assets/js/jquery.scrollUp.min.js');?>"</script>
    <script src="<?php echo base_url('assets/js/wow.min.js');?>"</script>
    <script src="<?php echo base_url('assets/js/nice-select.min.js');?>"</script>
    <script src="<?php echo base_url('assets/js/jquery.slicknav.min.js');?>"</script>
    <script src="<?php echo base_url('assets/js/jquery.magnific-popup.min.js');?>"</script>
    <script src="<?php echo base_url('assets/js/plugins.js');?>"</script>
    <script src="<?php echo base_url('assets/js/gijgo.min.js');?>"</script>
    <script src="<?php echo base_url('assets/js/slick.min.js');?>"</script>
    <!--contact js-->
 <!--    <script src="<?php echo base_url('assets/js/contact.js');?>"</script> -->
    <script src="<?php echo base_url('assets/js/jquery.ajaxchimp.min.js');?>"</script>
    <script src="<?php echo base_url('assets/js/jquery.form.js');?>"</script>
    <script src="<?php echo base_url('assets/js/jquery.validate.min.js');?>"</script>
    <script src="<?php echo base_url('assets/js/mail-script.js');?>"</script>
<!--     <script src="<?php echo base_url('assets/js/main.js');?>"></script> -->

</body>

</html>