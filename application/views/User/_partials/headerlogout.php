<body>
    
    <!-- header-start -->
    <header>
        <div class="header-area ">
            <div id="sticky-header" class="main-header-area" style="background-color:black;">
                <div class="container">
                    <div class="header_bottom_border">
                        <div class="row align-items-center">
                            <div class="col-xl-3 col-lg-2">
                                <div class="logo">
                                    <a href="<?php echo base_url();?>home/index">
                                        <img src="<?php echo base_url('assets/img/logo.png') ?>" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-7">
                                <div class="main-menu  d-none d-lg-block">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a class="active" href="<?php echo base_url()?>Home_controller">Beranda</a></li>
                                            <li><a href="<?php echo base_url();?>investsaya">Investasi Saya</a></li>
                                            <li><a href="<?php echo base_url();?>investsaya/bayar">Bayar</a></li>
                                            <li><a href="<?php echo base_url();?>landing/profilku">Profil</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 d-none d-lg-block">
                                <div class="Appointment">
                                    
                                    <div class="book_btn d-none d-lg-block">
                                        <a class="" href="<?php echo base_url()?>logout">Logout</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </header>