
    <!-- header-end -->

    <!-- slider_area_start -->
    
    <!-- slider_area_end -->

    <!-- service_area_start -->
     <div class="service_area">
        <div class="container">
        
            <div class="row" >

            <?php foreach ($home as $inv): ?>
                <div class="column">
                  
                    <div class="card">
                         <h2><?php echo $inv->J_INVEST ?> </h2>
                           <img src=<?php echo $inv->FOTO_HEWAN?> alt="Sapi" style="width: 100%">
                            <div class="container1">

                                <?php echo $this->session->userdata('nama'); ?>
                                    <p>Periode: <?php echo $inv->PERIODE_INVEST ?> </p>
                                    <p>Perkiraan Harga Beli: RP. <?php echo $inv->HARGA_BELI_PERKIRAAN ?></p>
                                    <p>Perkiraan Harga Jual: RP. <?php echo $inv->HARGA_JUAL_PERKIRAAN ?></p>
                                    <p>Perkiraan Biaya Makan: RP. <?php echo $inv->BIAYA_MAKAN ?></p>
                                    <p>Perkiraan Biaya Pegawai: RP. <?php echo $inv->BIAYA_GAJI ?></p>
                                    <p>Bobot Awal: <?php echo $inv->BOBOT_AWAL ?></p> 
                                    <p><button class="button" data-toggle="modal"  data-target="#modal_info<?php echo $inv->ID_INVEST;?>">Investasi</button></p>    
                                    </div>
                  
                </div>
                
            </div>
            <?php endforeach; ?>
        </div>

    </div>
    </div>

    <!-- service_area_end -->

    <!-- counter  -->
    <div class="counter_area">
        <div class="container">
            <div class="row">
                <div class="col-xlq-4 col-md-4">
                    <div class="single_counter text-center">
                        <h3> <span class="counter" >520</span> <span>+</span> </h3>
                        <span>Total Projects</span>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4">
                    <div class="single_counter text-center">
                        <h3> <span class="counter" >244</span>  </h3>
                        <span>On Going Projects</span>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4">
                    <div class="single_counter text-center">
                        <h3> <span class="counter" >95</span> <span>%</span> </h3>
                        <span>Job Success</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ counter  -->
<?php $this->load->view("user/_partials/modal.php") ?>

   

