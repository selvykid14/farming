
    <!-- footer start -->
    <footer class="footer">
        <div class="footer_top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-md-6 col-lg-4">
                        <div class="footer_widget">
                            <div class="footer_logo">
                                <a href="#">
                                    <img src="<?php echo base_url('assets/img/none.png')?>" alt="">
                                </a>
                            </div>
                            <p>
                                Official Social Media
                            </p>
                            <div class="socail_links">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <i class="ti-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="ti-twitter-alt"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-2 offset-xl-1 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                            </h3>
                            <ul>
                            </ul>

                        </div>
                    </div>
                    <div class="col-xl-2 col-md-6 col-lg-2">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                            </h3>
                            <ul>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Alamat
                            </h3>
                            <ul>
                                    <li>Jl. Palem Selatan II Blok MC No. 165, Wadungsari, Waru, Sidoarjo, Jawa Timur 61256</li>
                                    <li>(031) 8410298</li>
                                    <li><a href="#"> </a></li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right_text">
            <div class="container">
                <div class="footer_border"></div>
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right text-center">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script>  <i class="" aria-hidden="true"></i> by E FARMING</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/ footer end  -->

    <!-- link that opens popup -->

    <!-- form itself end-->
    <form id="test-form" class="white-popup-block mfp-hide">
        <div class="popup_box ">
            <div class="popup_inner">
                <div class="popup_header">
                    <h3>Log In</h3>
                </div>
                <div class="custom_form">
                        <div class="row">
                                <div class="col-xl-12">
                                    <input type="text" placeholder="Email">
                                </div>
                                <div class="col-xl-12">
                                    <input type="email" placeholder="Password">
                                </div>                                
                                <div class="col-xl-12">
                                    <button type="submit" class="boxed-btn3">Submit</button>
                                </div>
                            </div>
                </div>
				 <div class="text-center">
                    <a class="small" href="<?php echo base_url('assets/registrasi.html')?>">Create an Account!</a>
                  </div>
				  <hr>
				  
            </div>
        </div>
    </form>
    <!-- form itself end -->

    <!-- JS here -->
    <script src="<?php echo base_url('assets/js/vendor/modernizr-3.5.0.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/vendor/jquery-1.12.4.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/popper.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/owl.carousel.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/isotope.pkgd.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/ajax-form.js')?>"></script>
    <script src="<?php echo base_url('assets/js/waypoints.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.counterup.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/imagesloaded.pkgd.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/scrollIt.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.scrollUp.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/wow.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/nice-select.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.magnific-popup.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/plugins.js')?>"></script>
    <script src="<?php echo base_url('assets/js/gijgo.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
    <!--contact js-->
    <script src="<?php echo base_url('asset/js/contact.js')?>"></script>
    <script src="<?php echo base_url('asset/js/jquery.ajaxchimp.min.js')?>"></script>
    <script src="<?php echo base_url('asset/js/jquery.form.js')?>"></script>
    <script src="<?php echo base_url('asset/js/jquery.validate.min.js')?>"></script>
    <script src="<?php echo base_url('asset/js/mail-script.js')?>"></script>

    <script src="<?php echo base_url('asset/js/main.js')?>"></script>
   
</body>

</html>