<hr>
  <div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5 mx-auto">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        
        <div class="row" >
          <div class="col-lg-2"></div>                  
          <div class="col-lg-8">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
              </div>
              
              <?php echo form_open_multipart('proses/registrasi');?>
              <form class="user" method="post" action="<?= base_url('home/registrasi')?>">
                <div class="form-group">
                  <input type="text" class="form-control form-control-user" 
                  name="NAMA_MEMBER" id="NAMA_MEMBER" placeholder="Full Name">

                  <?= form_error('name','<small class="text-danger pl-3">','</small>'); ?>

                </div>
                <div class="form-group">
                  <input type="text" class="form-control form-control-user" name="EMAIL_MEMBER" id="EMAIL_MEMBER" placeholder="Email Address">
                  
                  <?= form_error('email','<small class="text-danger pl-3 ">','</small>'); ?>

                </div>
                <div class="form-group">
                  <input type="text" class="form-control form-control-user" name="ALAMAT_MEMBER" id="ALAMAT_MEMBER" placeholder="Alamat" value="<?= set_value('instansi'); ?>">
                  
                  <?= form_error('instansi','<small class="text-danger pl-3 ">','</small>'); ?>

                </div>
                <div class="form-group">
                  <input type="number" class="form-control form-control-user" name="NO_TELEPON_MEMBER" id="NO_TELEPON_MEMBER" placeholder="No. Handphone">
                  
                  <?= form_error('no_hp','<small class="text-danger pl-3 ">','</small>'); ?>

                </div>
                
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">

                    <input type="password" class="form-control form-control-user" 
                    id="PASSWORD_MEMBER" name="PASSWORD_MEMBER" placeholder="Password">

                    <?= form_error('password1','<small class="text-danger pl-3 ">','</small>'); ?>
                  </div>

                  <div class="col-sm-6">
                    <input type="password" class="form-control form-control-user" 
                    id="PASSWORD_MEMBER1" name="PASSWORD_MEMBER1" placeholder="Repeat Password">                    
                  </div>
                  
                </div>
                <button type="submit" class="btn btn-primary btn-user btn-block">
                  Register Account
                </button>
			  	<?php echo form_close();?> 
        </form>

              <hr>   
                  <div class="text-center">
                    <a class="small" href="<?= base_url();?>landing/login">Sudah Punya Akun? </a>
                  </div>     
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</hr>
  