<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class MY_Controller extends CI_Controller {

		public function cek_login()	{
    		if (!$this->session->userdata('username')) {
      			redirect(base_url("login"));
    		}
    	}

    	public function header(){ 
      if ($this->session->userdata('username')!=null) {
          $this->load->view("_partials/headerlogout.php");
        }else{
          $this->load->view("_partials/headerlogin.php");
        }


    }
    }