<?php if(!defined('BASEPATH')) exit ('No direct script access allowed');

class Home_model extends CI_Model {

     public function __construct()
     {
          parent::__construct();
     }

     function tampilhome(){
        return $this->db->get('jenis_invest');
    }
     function tampilbayar(){
        return $this->db->get('v_bayar');
    }

     function tampilinvest(){
        return $this->db->get('v_invest');
    }

    function tampilprofilku(){
        return $this->db->get('user_member');
    }

    function save($data){
            $this->db->insert('transaksi', $data);
        }

function get_investsaya(){
            $data = array('ID_MEMBER' => $ID_MEMBER, 'status' => 1);
            $query = $this->db->where($data);
            return $query->result();
        }


function get_trid(){
            $this->db->select_max('ID_TRANSAKSI', 'ID_TRANSAKSI');
            $this->db->from('transaksi');
            $query = $this->db->get();
            return $query->result();
        }

    //  public function getall()
    // {
    //     $query = $this->db->select("*")
    //              ->from('jenis_invest')
    //              ->order_by('id_invest', 'DESC')
    //              ->get();
    //     return $query->result();
    // }
    // public function getById($id)
    // {
    //     return $this->db->get_where('jenis_invest', ["ID_INVEST" => $id])->row();
    // }

 
    // public function save()
    // {
    //     $post = $this->input->post();
    //     $this->ID_INVEST = uniqid();
    //     $this->J_INVEST = $post["J_INVEST"];
    //     $this->JUMLAH_INVEST = $post["JUMLAH_INVEST"];
    //     $this->PERIODE_INVEST = $post["PERIODE_INVEST"];
    //     $this->BIAYA_MAKAN = $post["BIAYA_MAKAN"];
    //     $this->BIAYA_GAJI = $post["BIAYA_GAJI"];
    //     $this->HARGA_JUAL_PERKIRAAN = $post["HARGA_JUAL_PERKIRAAN"];
    //     $this->HARGA_BELI_PERKIRAAN = $post["HARGA_BELI_PERKIRAAN"];
    //     $this->BOBOT_AWAL = $post["BOBOT_AWAL"];
    //     $this->FOTO_HEWAN = $post["FOTO_HEWAN"];
    //     $this->Keterangan_Invest = $post["Keterangan_Invest"];

    //     return $this->db->insert('jenis_invest', $this);
    // }

    // public function update()
    // {
    //     $post = $this->input->post();
    //     $this->ID_INVEST = $post["ID_INVEST"];
    //      $this->J_INVEST = $post["J_INVEST"];
    //     $this->JUMLAH_INVEST = $post["JUMLAH_INVEST"];
    //     $this->PERIODE_INVEST = $post["PERIODE_INVEST"];
    //     $this->BIAYA_MAKAN = $post["BIAYA_MAKAN"];
    //     $this->BIAYA_GAJI = $post["BIAYA_GAJI"];
    //     $this->HARGA_JUAL_PERKIRAAN = $post["HARGA_JUAL_PERKIRAAN"];
    //     $this->HARGA_BELI_PERKIRAAN = $post["HARGA_BELI_PERKIRAAN"];
    //     $this->BOBOT_AWAL = $post["BOBOT_AWAL"];
    //     $this->FOTO_HEWAN = $post["FOTO_HEWAN"];
    //     $this->Keterangan_Invest = $post["Keterangan_Invest"];
    //     return $this->db->update('jenis_invest', $this, array('ID_INVEST' => $post['id']));
    // }

    // public function delete($id)
    // {
    //     return $this->db->delete('jenis_invest', array("ID_INVEST" => $id));
    // }
}