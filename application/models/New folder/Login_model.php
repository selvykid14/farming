<?php
	class Login_model extends CI_Model {

		//ambil tabel users

		public function cek_user($username, $password) {
			//cari username dan validsai
			$this->db->where('EMAIL_MEMBER', $username);
			$query = $this->db->get('user_member')->row();

			//user tidak ditemukan
			if(!$query) return 1;

			//user password salah 
			$hash = $query->PASSWORD_MEMBER;
			if (md5($password) != $hash) return 2;

			return $query;
		}

		function update($EMAIL_MEMBER, $time)
		{
			$this->db->set('member_lastlog', $time);
			$this->db->where("EMAIL_MEMBER", $EMAIL_MEMBER);
			$this->db->update('user_member');
		}

	}