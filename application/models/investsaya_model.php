<?php if(!defined('BASEPATH')) exit ('No direct script access allowed');

class investsaya_model extends CI_Model {

     public function __construct()
     {
          parent::__construct();
     }

    public function select($selectcolumn=true){
        
            if($selectcolumn){
            $this->db->select('tr.ID_INVEST');
            $this->db->select('J_INVEST');
            $this->db->select('KETERANGAN_TRANSAKSI');
            $this->db->select('JUMLAH_INVEST');
            $this->db->select('PERIODE_INVEST');
            $this->db->select('BIAYA_MAKAN');
            $this->db->select('BIAYA_GAJI');
            $this->db->select('HARGA_JUAL_PERKIRAAN');
            $this->db->select('HARGA_BELI_PERKIRAAN');
            $this->db->select('BOBOT_AWAL');
            $this->db->select('FOTO_HEWAN');
            $this->db->select('KETERANGAN_INVEST');
            $this->db->select('status');
            }
        $this->db->from('transaksi tr');
        $this->db->join('jenis_invest j_inv', 'j_inv.ID_INVEST = tr.ID_INVEST');
        

    }

    function get($where = "", $order = "ID_INVEST asc", $limit=null, $offset=null, $selectcolumn = true)
        {
            $id = $this->session->userdata('id_member');
            $this->select($selectcolumn);
            $this->db->where('status = 2 AND ID_MEMBER =',$id );
            if($limit != null) $this->db->limit($limit, $offset);
            
            $this->db->order_by($order);
            $query = $this->db->get();
            return $query->result();

        }

        function getbayar($where = "", $order = "ID_INVEST asc", $limit=null, $offset=null, $selectcolumn = true)
        {
            $id = $this->session->userdata('id_member');
            $this->select($selectcolumn);
            $this->db->where('status = 1 AND ID_MEMBER =',$id );
            if($limit != null) $this->db->limit($limit, $offset);
            
            $this->db->order_by($order);
            $query = $this->db->get();
            return $query->result();

        }

}