<?php

  	class Login extends CI_Controller {

    	function __construct() {
      		parent::__construct();
            $this->load->helper(array('url'));
            $this->load->helper('date');
       
    	}

        function index(){
            $this->load->view("_partials/head.php");
            $this->header();
            $this->login();
            $this->load->view("_partials/footer.php");
            
        }

    	function cek_user() {
    		// load model user
    		$this->load->model('Login_model');

    		//validasi login
    		$username = trim($this->input->post('username'));
			$password = trim($this->input->post('password'));
            
            $query = $this->Login_model->cek_user($username, $password);

            if ($query === 1) {
                return $username;
            }
            else if ($query === 2) 
            {
                return "Password Salah!";
            } 
            else 
            {

                //membuat session
                $user = array(
                    'username' => $query->EMAIL_MEMBER,
                    'id'    => $query->ID_MEMBER,
                    'alamat'=> $query->ALAMAT_MEMBER,
                    'nama' => $query->NAMA_MEMBER,
                    'logged_in' => TRUE
                );
                $this->session->set_userdata($user);
                return TRUE;
            }
    		
    	}

    	public function login() {
    		//redirect dashboard
    		if ($this->session->userdata('username')!=null) {
    			redirect(base_url("Home_controller"));
    		}

    		//$username = trim($this->input->post('username'));
			//$password = trim($this->input->post('password'));

    		//login dan validasi
    		if ($this->input->post('submit')) {
				$this->load->model('Login_model');
    			$this->form_validation->set_rules('username', 'Username', 'required');
      			$this->form_validation->set_rules('password', 'Password', 'required');
      			$error = $this->cek_user();
      			if ($this->form_validation->run() && $error === TRUE) {
        			$data = $this->Login_model->cek_user($username, $password);
        			//jika benar alihkan ke dashboard
        			redirect(base_url("Home_controller"));
        		}
        		else if ($error !== FALSE) {
        			$data['error'] = $error;
        			$this->load->view('dashboard/login', $data);
        		}
        	}
        	else {
        		$this->load->view('dashboard/login');
        	}

    	}

    	public function logout() {
            if ($this->session->userdata('username') == 'admin') {
                $this->session->sess_destroy();
                redirect(base_url());
            }else{
                $this->load->model('Login_model');
                $this->Login_model->update($this->session->userdata('username'), now());
                $this->session->sess_destroy();
                redirect(base_url());
            }
            
    	}
        

     public function header(){ 
      if ($this->session->userdata('username')!=null) {
          $this->load->view("_partials/headerlogout.php");
        }else{
          $this->load->view("_partials/headerlogin.php");
        }


    }

    }
