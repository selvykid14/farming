<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_controller extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
     {
          parent::__construct();

          $this->load->model('User_model');
          $this->load->helper('url');
     }

        public function index()
    {
        // $data = array(

        //     'title'  => 'Jenis invest',
        //     'invest' => $this->Home_model->getall(),

        // );

    	$data["home"] = $this->User_model->tampilhome()->result();
        $this->load->view('Home', $data);

    }
    

    public function investsaya(){

    	$data["investsaya"] = $this->User_model->tampilinvest()->result();
    	$this->load->view('investsaya', $data);
    }

    public function bayar(){
    	$data["bayar"] = $this->User_model->tampilbayar()->result();
    	$this->load->view('bayar',$data);
    }


    public function add(){
		$J_INVEST = $this->input->post('J_INVEST');
		$JUMLAH_INVEST = $this->input->post('JUMLAH_INVEST');
		$PERIODE_INVEST = $this->input->post('PERIODE_INVEST');
		$BIAYA_MAKAN = $this->input->post('BIAYA_MAKAN');
 		$BIAYA_GAJI = $this->input->post('BIAYA_GAJI');
 		$HARGA_JUAL_PERKIRAAN = $this->input->post('HARGA_JUAL_PERKIRAAN');
 		$HARGA_BELI_PERKIRAAN = $this->input->post('HARGA_BELI_PERKIRAAN');
 		$BOBOT_AWAL = $this->input->post('BOBOT_AWAL');
 		$FOTO_HEWAN = $this->input->post('FOTO_HEWAN');
 		$Keterangan_Invest = $this->input->post('Keterangan_Invest');

		$data = array(
			'J_INVEST' => $J_INVEST,
			'JUMLAH_INVEST' => $JUMLAH_INVEST,
			'PERIODE_INVEST' => $PERIODE_INVEST,
			'BIAYA_MAKAN' => $BIAYA_MAKAN,
			'BIAYA_GAJI' => $BIAYA_GAJI,
			'HARGA_JUAL_PERKIRAAN' => $HARGA_JUAL_PERKIRAAN,
			'HARGA_BELI_PERKIRAAN' => $HARGA_BELI_PERKIRAAN,
			'BOBOT_AWAL' => $BOBOT_AWAL,
			'FOTO_HEWAN' => $FOTO_HEWAN,
			'Keterangan_Invest' => $Keterangan_Invest

			);
		
		$this->Home_model->input_data($data,'jenis_invest');
		redirect('User');
	}

	// public function add_transaksi(){



	// 	$ID_INVEST = $this->input->post('ID_INVEST');
	// 	$ID_MEMBER = $this->session->userdata('ID_MEMBER')->post('ID_MEMBER'); 
	// 	$TANGGAL_TRANSAKSI = $this->input->post('TANGGAL_TRANSAKSI');
	// 	$KETERANGAN_TRANSAKSI = $this->input->post('KETERANGAN_TRANSAKSI');

	// 	$data = array(
	// 		'ID_INVEST' => $ID_INVEST,
	// 		'ID_MEMBER' => $ID_MEMBER,
	// 		'TANGGAL_TRANSAKSI' => $TANGGAL_TRANSAKSI,
	// 		'KETERANGAN_TRANSAKSI' => $KETERANGAN_TRANSAKSI

	// 		);
		
	// 	$this->User_model->input_data($data,'transaksi');
	// 	redirect('User');


	// }



}