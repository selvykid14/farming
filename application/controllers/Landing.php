<?php


class Landing extends CI_Controller {
	public function __construct()
	{
        parent::__construct();
        $this->load->model('Invest_model');
		$this->load->library('form_validation');
		$this->load->database();	

	}
	public function index()
	{
        $data['judul'] = 'E-FARMING';
        $this->load->view('partial/header1', $data);	
        $this->load->view('landing');		
        $this->load->view('partial/footer');
    }
    
    public function profil()
    {
        $data['judul'] = 'Profil E-Farming';
        $this->load->view('partial/header1', $data);	
        $this->load->view('profil');		
        $this->load->view('partial/footer');
    }

    public function kontak()
    {
        $data['judul'] = 'Hubungi Kami';
        $this->load->view('partial/header1', $data);	
        $this->load->view('kontak');		
        $this->load->view('partial/footer');
    }

    public function login()
    {
        $data['judul'] = 'Login';
        $this->load->view('partial/header1', $data);	
        $this->load->view('login');		
        $this->load->view('partial/footer');
    }

    function logout(){				
        $this->session->sess_destroy();
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('id_member');
			redirect('landing');	
			
	}

    public function registrasi()
    {
        $data['judul'] = 'Registrasi Akun';
        $this->load->view('partial/header1', $data);	
        $this->load->view('registrasi');		
        $this->load->view('partial/footer');
    }

    public function deskripsi1()
    {
        $data['judul'] = 'Promosi 1';
        $this->load->view('partial/header1', $data);	
        $this->load->view('deskripsi1');		
        $this->load->view('partial/footer');
    }

    public function deskripsi2()
    {
        $data['judul'] = 'Promosi 2';
        $this->load->view('partial/header1', $data);	
        $this->load->view('deskripsi2');		
        $this->load->view('partial/footer');
    }

    public function deskripsi3()
    {
        $data['judul'] = 'Promosi 3';
        $this->load->view('partial/header1', $data);	
        $this->load->view('deskripsi3');		
        $this->load->view('partial/footer');
    }

    public function pembayaran()
    {
        $data['judul'] = 'Pembayaran Investasi';
        $this->load->view('partial/header2', $data);	
        $this->load->view('pembayaran');		
        $this->load->view('partial/footer');
    }

    public function upload_pembayaran()
    {
        $data['judul'] = 'Pembayaran Investasi';
        $this->load->view('partial/header2', $data);	
        $this->load->view('upload_pembayaran');		
        $this->load->view('partial/footer');
    }

    public function profilku()
    {
        $id = $this->session->userdata('id_member');
        $this->load->model('Invest_model');
        $profil['data'] = $this->Invest_model->profil($id);
        
        $data['judul'] = 'Profil Saya';
        $this->load->view('partial/header2', $data);	
        $this->load->view('profilku', $profil);		
        $this->load->view('partial/footer');

    }
    public function home()
    {
        $id = $this->session->userdata('id_member');
        $this->load->model('Home_model');
        
        $home["home"] = $this->Home_model->tampilhome()->result();
        $data['judul'] = 'Home';
        $this->load->view('partial/header2', $data);    
        $this->load->view('User/Home', $home);     
        $this->load->view('partial/footer');

    }

    public function investsaya()
    {
        $id = $this->session->userdata('id_member');
        $this->load->model('Home_model');
        
        $home["home"] = $this->Home_model->tampilhome()->result();
        $data['judul'] = 'Home';
        $this->load->view('partial/header2', $data);    
        $this->load->view('User/investsaya', $home);     
        $this->load->view('partial/footer');

    }
}