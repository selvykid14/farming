<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* This is Example Controller
*/
class investsaya extends CI_Controller
{

	function __construct() {
		parent::__construct();

      	$this->load->model('investsaya_model');
	}

	function index() {

    $data["investsaya"] = $this->investsaya_model->get();
    $this->load->view("user/_partials/head.php");
    $this->header();
    $this->load->view('user/investsaya', $data);
    $this->load->view("user/_partials/footer.php");

	}
 public function header(){ 
      if ($this->session->userdata('id_member')!=null) {
          $this->load->view("user/_partials/headerlogout.php");
        }else{
          $this->load->view("user/_partials/headerlogin.php");
        }


    }
     function bayar(){

      $data["bayar"] = $this->investsaya_model->getbayar();
      $this->load->view("user/_partials/head.php");
      $this->header();
      $this->load->view('user/bayar',$data);
      $this->load->view("user/_partials/footer.php");
    }




  

	
}