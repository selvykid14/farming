<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* This is Example Controller
*/
class Home_controller extends CI_Controller
{

	function __construct() {
		parent::__construct();
      
      	$this->load->model('Home_model');
	}

	function index() {
        
        $id = $this->session->userdata('id_member');
        $data["home"] = $this->Home_model->tampilhome()->result();
    $this->load->view("user/_partials/head.php");
    $this->header();
    $this->load->view('user/Home', $data);
    $this->load->view("user/_partials/footer.php");


	}



      function save(){
        $data = $this->Home_model->get_trid();
      $TR_ID = '';
      foreach ($data as $row) {
        $TR_ID = ((int)$row->ID_TRANSAKSI+1);
      }

            $data = array(
                'ID_TRANSAKSI'          => $TR_ID,
                'ID_INVEST'             => $this->input->post('id_invest'),
                'ID_MEMBER'             => $this->session->userdata('id'),
                'TANGGAL_TRANSAKSI'      => date('Y-m-d H:i:s'),
                'KETERANGAN_TRANSAKSI'  => $this->input->post('slot'),
                'status'                => 2
                
            ); 


            $this->Home_model->save($data);
            redirect(base_url("Home_controller"));
        }

	 function investsaya(){

    	$data["investsaya"] = $this->Home_model->tampilinvest()->result();
      $this->load->view("_partials/head.php");
      $this->header();
    	$this->load->view('laporan/investsaya', $data);
      $this->load->view("_partials/footer.php");
    }

     function bayar(){

    	$data["bayar"] = $this->Home_model->tampilbayar()->result();
      $this->load->view("_partials/head.php");
      $this->header();
    	$this->load->view('transaksi/bayar',$data);
      $this->load->view("_partials/footer.php");
    }


    function profilku(){

      $data["profilku"] = $this->Home_model->tampilprofilku()->result();
      $this->load->view("_partials/head.php");
      $this->header();
      $this->load->view('about/profilku',$data);
      $this->load->view("_partials/footer.php");
    }
    
    public function header(){ 
      if ($this->session->userdata('id_member')!=null) {
          $this->load->view("user/_partials/headerlogout.php");
        }else{
          $this->load->view("user/_partials/headerlogin.php");
        }


    }
	
}