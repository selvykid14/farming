<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proses extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('Invest_model');
		$this->load->model('Upload_model');
		$this->load->library('session', 'upload');
		$this->load->helper(array('form','url','file','download','text','language'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		echo "<script>
			var base_url = 'http://localhost/farming/';
		</script>";
	}
	
	public function registrasi(){
		$id=$this->input->post('ID_MEMBER');
		$nama=$this->input->post('NAMA_MEMBER');
		$email=$this->input->post('EMAIL_MEMBER');
		$nomor=$this->input->post('ALAMAT_MEMBER');
		$password=$this->input->post('PASSWORD_MEMBER');
		$password1=$this->input->post('PASSWORD_MEMBER1');
		if($nama == ''||$nomor == ''||$email == ''){
		echo "<script>alert('Nama, Nomor Telepon, Email tidak boleh kosong');history.go(-1);</script>";
		}else if($password != $password1){
		echo "<script>alert('Konfirmasi password tidak sama');history.go(-1);</script>";
		}else if($id == ''){
		$nama=$this->input->post('NAMA_MEMBER');
		$email=$this->input->post('EMAIL_MEMBER');
		$alamat=$this->input->post('ALAMAT_MEMBER');
		$nomor=$this->input->post('NO_TELEPON_MEMBER');
		$password=$this->input->post('PASSWORD_MEMBER');

		$nama = str_replace(str_split('`!@#$%^&*()"+|}{:?><-=[]\;/~'), "", $nama);
		
		$this->session->set_flashdata('message','<div class="alert
			alert-success" role="alert">Selamat! Akun anda sudah terdaftar. Silahkan Login</div>');	
		$this->Invest_model->registrasi($id,$nama,$email,$alamat,$nomor,$password);
		echo "<script>alert('Registrasi berhasil, Silahkan login');
		window.location = base_url+'landing/login';
		</script>";
		}
	}
	
	public function cek_login() {
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$hasil = $this->Invest_model->cek_user($email,$password);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$data['logged_in'] 		= 'sudah login';
				$data['id_member'] 		= $sess->ID_MEMBER;
				$this->session->set_userdata($data);
				
				
			}
		
			if ($this->session->userdata('logged_in')=='sudah login') {
				echo "<script>alert('Login berhasil');
		window.location = base_url+'Home_controller';
		</script>";
			}
			
		}else {
			echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
		}
	}
	
	function logout(){
	   $this->session->unset_userdata('logged_in');
	   session_destroy();
	   redirect('landing', 'refresh');
	}
	
	public function login(){
		$email = $this->input->post('email');
		$pass = $this->input->post('password');
		$count = $this->Invest_model->login($email,$pass);
		if ($count == 1) {
			redirect('Home_controller');
		}else{
			$this->load->view('home/header1');
			$this->load->view('home/profilku');
			$this->load->view('home/footer');
		}
	}

	public function pembayaran(){

	}

	public function upload_pembayaran(){
		$id=$this->input->post('ID_TRANSAKSI');
		$bukti=$this->input->post('BUKTI_PEMBAYARAN');
	
		$this->Invest_model->upload_pembayaran($id,$bukti);
		echo "<script>alert('Unggah bukti pembayaran investasi berhasil');
		window.location = base_url+'landing/index';
		</script>";
	}

	public function do_upload(){
		//$path = ROOT_UPLOAD_PATH;

		$initialize = $this->upload->initialize(array(
            "upload_path" => "./assets/img/upload",
            "allowed_types" => "gif|jpg|jpeg|png|bmp",
            "encrypt_name" => TRUE
		));
		//$this->load->library('upload',$config); //call library upload 
        if($this->upload->do_upload("file")){ //upload file
            $data = array('upload_data' => $this->upload->data()); //ambil file name yang diupload
 
            //$judul= $this->input->post('judul'); //get judul image
            $image= $data['upload_data']['file_name']; //set file name ke variable image
             
            $result= $this->Upload_model->simpan_upload($image); //kirim value ke model
            echo json_decode($result);
        }
          
	}
	
	public function uploadBukti()
	{
		$type = explode('.', $_FILES['BUKTI_PEMBAYARAN']['name']);
		$type = $type[count($type)-1];
		$foto = "./assets/img/upload/".uniqid(rand()).'.'.$type;
		if(in_array($type, array('gif','jpg','jpeg','png','bmp')))
			if(is_uploaded_file($_FILES['BUKTI_PEMBAYARAN']['tmp_name']))
				if(move_uploaded_file($_FILES['BUKTI_PEMBAYARAN']['tmp_name'], $foto))
					return $foto;
		return "";	
	} 

	public function uploadFoto()
	{
		$type = explode('.', $_FILES['FOTO_MEMBER']['name']);
		$type = $type[count($type)-1];
		$foto = "./assets/img/upload/".uniqid(rand()).'.'.$type;
		if(in_array($type, array('gif','jpg','jpeg','png','bmp')))
			if(is_uploaded_file($_FILES['FOTO_MEMBER']['tmp_name']))
				if(move_uploaded_file($_FILES['FOTO_MEMBER']['tmp_name'], $foto))
					return $foto;
		return "";
	}

	public function saveUpload()
	{
		$foto = $this->uploadBukti();
		$this->Upload_model->simpan_upload($foto);
		redirect('landing/profilku');
	}

	public function simpanFoto()
	{
		$id = $this->session->userdata('id_member');
		$foto = $this->uploadFoto();
		$hasil = $this->Upload_model->simpan_foto($foto, $id);
		redirect('landing/profilku');
	}


	 public function simpan()
    {
      $id = $this->session->userdata('id_member');
      
        $updated_data = array(
          'NAMA_MEMBER'     	=> $this->input->post('nama'),
          'NO_KTP_MEMBER'   		=> $this->input->post('no_ktp'),
          'NO_TELEPON_MEMBER'      	=> $this->input->post('no_telp'),
          'ALAMAT_MEMBER'			=> $this->input->post('alamat'),
          'NO_REKENING_MEMBER'		=> $this->input->post('no_rek'),
          'EMAIL_MEMBER'			=> $this->input->post('email'),
          'USERNAME_MEMBER'			=> $this->input->post('usernam')
        );
        $this->Upload_model->update($this->input->post("id"), $updated_data);
        redirect('landing/profilku');
      }
      
    



}
?>