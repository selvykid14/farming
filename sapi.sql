-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Feb 2020 pada 01.16
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sapi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasil_penjualan`
--

CREATE TABLE `hasil_penjualan` (
  `ID_HASIL` int(11) NOT NULL,
  `ID_TRANSAKSI` int(11) DEFAULT NULL,
  `NAMA_HASIL` varchar(20) DEFAULT NULL,
  `JUMLAH` varchar(20) DEFAULT NULL,
  `BUKTI_PENJUALAN` varchar(225) DEFAULT NULL,
  `TANGGAL_PENJUALAN` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `hewan`
--

CREATE TABLE `hewan` (
  `ID_HEWAN` int(11) NOT NULL,
  `JENIS_HEWAN` varchar(10) DEFAULT NULL,
  `JENIS_KELAMIN` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hewan`
--

INSERT INTO `hewan` (`ID_HEWAN`, `JENIS_HEWAN`, `JENIS_KELAMIN`) VALUES
(1, 'Sapi', 'Jantan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_invest`
--

CREATE TABLE `jenis_invest` (
  `ID_INVEST` int(11) NOT NULL,
  `ID_HEWAN` int(11) NOT NULL,
  `J_INVEST` varchar(20) DEFAULT NULL,
  `JUMLAH_INVEST` varchar(20) DEFAULT NULL,
  `PERIODE_INVEST` varchar(50) DEFAULT NULL,
  `BIAYA_MAKAN` varchar(20) DEFAULT NULL,
  `BIAYA_GAJI` varchar(20) DEFAULT NULL,
  `HARGA_JUAL_PERKIRAAN` varchar(20) DEFAULT NULL,
  `HARGA_BELI_PERKIRAAN` varchar(20) DEFAULT NULL,
  `BOBOT_AWAL` varchar(5) DEFAULT NULL,
  `FOTO_HEWAN` varchar(225) DEFAULT NULL,
  `Keterangan_Invest` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_invest`
--

INSERT INTO `jenis_invest` (`ID_INVEST`, `ID_HEWAN`, `J_INVEST`, `JUMLAH_INVEST`, `PERIODE_INVEST`, `BIAYA_MAKAN`, `BIAYA_GAJI`, `HARGA_JUAL_PERKIRAAN`, `HARGA_BELI_PERKIRAAN`, `BOBOT_AWAL`, `FOTO_HEWAN`, `Keterangan_Invest`) VALUES
(1, 1, 'Sapi', '2000000', 'juni-agustus', '2000000', '3000000', '30000000', '20000000', '150', NULL, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_makanan`
--

CREATE TABLE `jenis_makanan` (
  `ID_MAKANAN` int(11) NOT NULL,
  `NAMA_MAKANAN` varchar(20) DEFAULT NULL,
  `HARGA_MAKANAN` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `laporan_akhir`
--

CREATE TABLE `laporan_akhir` (
  `ID_HASIL` int(11) DEFAULT NULL,
  `ID_TRANSAKSI` int(11) DEFAULT NULL,
  `HARGA_JUAL` varchar(20) NOT NULL,
  `TOTAL_BIAYA_PENGELOLAAN` varchar(20) DEFAULT NULL,
  `HARGA_BELI` varchar(20) DEFAULT NULL,
  `PROFIT` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `laporan_harian`
--

CREATE TABLE `laporan_harian` (
  `ID_PEGAWAI` int(11) DEFAULT NULL,
  `ID_MAKANAN` int(11) DEFAULT NULL,
  `ID_TRANSAKSI` int(11) DEFAULT NULL,
  `HARI` varchar(10) NOT NULL,
  `TANGGAL` date NOT NULL,
  `BOBOT_BULANAN` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran`
--

CREATE TABLE `pembayaran` (
  `ID_TRANSAKSI` int(11) DEFAULT NULL,
  `BUKTI_PEMBAYARAN` varchar(225) DEFAULT NULL,
  `KETERANGAN_BAYAR` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_user`
--

CREATE TABLE `role_user` (
  `ID_Role` int(11) NOT NULL,
  `ROLE` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `ID_TRANSAKSI` int(11) NOT NULL,
  `ID_INVEST` int(11) DEFAULT NULL,
  `ID_MEMBER` int(11) DEFAULT NULL,
  `TANGGAL_TRANSAKSI` date DEFAULT NULL,
  `KETERANGAN_TRANSAKSI` varchar(225) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`ID_TRANSAKSI`, `ID_INVEST`, `ID_MEMBER`, `TANGGAL_TRANSAKSI`, `KETERANGAN_TRANSAKSI`, `status`) VALUES
(1, 1, 1, '2020-02-26', '30000000', 2),
(2, 1, 1, '2020-02-11', '2000000', 1),
(3, 1, NULL, '2020-02-13', '2000000', 1),
(4, 1, 1, '2020-02-13', '2000500', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_admin`
--

CREATE TABLE `user_admin` (
  `ID_ADMIN` int(11) NOT NULL,
  `ADMIN` int(11) DEFAULT NULL,
  `PEGAWAI` int(11) DEFAULT NULL,
  `MEMBER` int(11) DEFAULT NULL,
  `NO_KTP_ADMIN` varchar(20) DEFAULT NULL,
  `NAMA_ADMIN` varchar(225) DEFAULT NULL,
  `NO_TELEPON_ADMIN` varchar(20) DEFAULT NULL,
  `ALAMAT_ADMIN` varchar(225) DEFAULT NULL,
  `NO_REKENING_ADMIN` varchar(20) DEFAULT NULL,
  `EMAIL_ADMIN` varchar(225) DEFAULT NULL,
  `PASSWORD_ADMIN` varchar(10) DEFAULT NULL,
  `USERNAME_ADMIN` varchar(20) DEFAULT NULL,
  `FOTO_ADMIN` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_member`
--

CREATE TABLE `user_member` (
  `NAMA_MEMBER` varchar(225) DEFAULT NULL,
  `NO_KTP_MEMBER` int(11) DEFAULT NULL,
  `NO_TELEPON_MEMBER` varchar(20) DEFAULT NULL,
  `ALAMAT_MEMBER` varchar(225) DEFAULT NULL,
  `NO_REKENING_MEMBER` varchar(20) DEFAULT NULL,
  `EMAIL_MEMBER` varchar(20) DEFAULT NULL,
  `PASSWORD_MEMBER` varchar(225) DEFAULT NULL,
  `USERNAME_MEMBER` varchar(20) DEFAULT NULL,
  `ID_MEMBER` int(11) NOT NULL,
  `FOTO_MEMBER` varchar(225) DEFAULT NULL,
  `member_lastlog` datetime NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_member`
--

INSERT INTO `user_member` (`NAMA_MEMBER`, `NO_KTP_MEMBER`, `NO_TELEPON_MEMBER`, `ALAMAT_MEMBER`, `NO_REKENING_MEMBER`, `EMAIL_MEMBER`, `PASSWORD_MEMBER`, `USERNAME_MEMBER`, `ID_MEMBER`, `FOTO_MEMBER`, `member_lastlog`, `status`) VALUES
('abc', NULL, '08263723623', 'abc@gmail.com', NULL, 'abc', '202cb962ac59075b964b07152d234b70', NULL, 1, NULL, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_pegawai`
--

CREATE TABLE `user_pegawai` (
  `ID_PEGAWAI` int(11) NOT NULL,
  `ADMIN` int(11) DEFAULT NULL,
  `PEGAWAI` int(11) DEFAULT NULL,
  `MEMBER` int(11) DEFAULT NULL,
  `NO_KTP_PEGAWAI` varchar(20) DEFAULT NULL,
  `NAMA_PEGAWAI` varchar(225) DEFAULT NULL,
  `NO_TELEPON_PEGAWAI` varchar(20) DEFAULT NULL,
  `ALAMAT_PEGAWAI` varchar(225) DEFAULT NULL,
  `NO_REKENING_PEGAWAI` varchar(20) DEFAULT NULL,
  `EMAIL_PEGAWAI` varchar(20) DEFAULT NULL,
  `PASSWORD_PEGAWAI` varchar(10) DEFAULT NULL,
  `USERNAME_PEGAWAI` varchar(10) DEFAULT NULL,
  `FOTO_PEGAWAI` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `hasil_penjualan`
--
ALTER TABLE `hasil_penjualan`
  ADD PRIMARY KEY (`ID_HASIL`),
  ADD UNIQUE KEY `HASIL_PENJUALAN_PK` (`ID_HASIL`),
  ADD KEY `RELATIONSHIP_11_FK` (`ID_TRANSAKSI`);

--
-- Indeks untuk tabel `hewan`
--
ALTER TABLE `hewan`
  ADD PRIMARY KEY (`ID_HEWAN`);

--
-- Indeks untuk tabel `jenis_invest`
--
ALTER TABLE `jenis_invest`
  ADD PRIMARY KEY (`ID_INVEST`),
  ADD UNIQUE KEY `JENIS_INVEST_PK` (`ID_INVEST`),
  ADD KEY `ID_HEWAN` (`ID_HEWAN`),
  ADD KEY `ID_HEWAN_2` (`ID_HEWAN`),
  ADD KEY `ID_HEWAN_3` (`ID_HEWAN`);

--
-- Indeks untuk tabel `jenis_makanan`
--
ALTER TABLE `jenis_makanan`
  ADD PRIMARY KEY (`ID_MAKANAN`),
  ADD UNIQUE KEY `JENIS_MAKANAN_PK` (`ID_MAKANAN`);

--
-- Indeks untuk tabel `laporan_akhir`
--
ALTER TABLE `laporan_akhir`
  ADD KEY `RELATIONSHIP_9_FK` (`ID_TRANSAKSI`),
  ADD KEY `RELATIONSHIP_10_FK` (`ID_HASIL`);

--
-- Indeks untuk tabel `laporan_harian`
--
ALTER TABLE `laporan_harian`
  ADD KEY `RELATIONSHIP_15_FK` (`ID_PEGAWAI`),
  ADD KEY `RELATIONSHIP_17_FK` (`ID_TRANSAKSI`),
  ADD KEY `RELATIONSHIP_18_FK` (`ID_MAKANAN`);

--
-- Indeks untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD KEY `RELATIONSHIP_16_FK` (`ID_TRANSAKSI`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`ID_TRANSAKSI`),
  ADD UNIQUE KEY `TRANSAKSI_PK` (`ID_TRANSAKSI`),
  ADD KEY `RELATIONSHIP_5_FK` (`ID_INVEST`),
  ADD KEY `ID_MEMBER` (`ID_MEMBER`);

--
-- Indeks untuk tabel `user_admin`
--
ALTER TABLE `user_admin`
  ADD PRIMARY KEY (`ID_ADMIN`),
  ADD UNIQUE KEY `USER_ADMIN_PK` (`ID_ADMIN`),
  ADD KEY `RELATIONSHIP_13_FK` (`ADMIN`,`PEGAWAI`,`MEMBER`);

--
-- Indeks untuk tabel `user_member`
--
ALTER TABLE `user_member`
  ADD PRIMARY KEY (`ID_MEMBER`),
  ADD UNIQUE KEY `USER_MEMBER_PK` (`ID_MEMBER`);

--
-- Indeks untuk tabel `user_pegawai`
--
ALTER TABLE `user_pegawai`
  ADD PRIMARY KEY (`ID_PEGAWAI`),
  ADD UNIQUE KEY `USER_PEGAWAI_PK` (`ID_PEGAWAI`),
  ADD KEY `RELATIONSHIP_14_FK` (`ADMIN`,`PEGAWAI`,`MEMBER`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `user_member`
--
ALTER TABLE `user_member`
  MODIFY `ID_MEMBER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `hasil_penjualan`
--
ALTER TABLE `hasil_penjualan`
  ADD CONSTRAINT `FK_HASIL_PE_RELATIONS_TRANSAKSI` FOREIGN KEY (`ID_TRANSAKSI`) REFERENCES `transaksi` (`ID_TRANSAKSI`);

--
-- Ketidakleluasaan untuk tabel `jenis_invest`
--
ALTER TABLE `jenis_invest`
  ADD CONSTRAINT `jenis_invest_ibfk_1` FOREIGN KEY (`ID_HEWAN`) REFERENCES `hewan` (`ID_HEWAN`);

--
-- Ketidakleluasaan untuk tabel `laporan_akhir`
--
ALTER TABLE `laporan_akhir`
  ADD CONSTRAINT `FK_LAPORAN__RELATIONS_HASIL_PE` FOREIGN KEY (`ID_HASIL`) REFERENCES `hasil_penjualan` (`ID_HASIL`),
  ADD CONSTRAINT `FK_LAPORAN__RELATIONS_TRANSAKSI` FOREIGN KEY (`ID_TRANSAKSI`) REFERENCES `transaksi` (`ID_TRANSAKSI`);

--
-- Ketidakleluasaan untuk tabel `laporan_harian`
--
ALTER TABLE `laporan_harian`
  ADD CONSTRAINT `FK_LAPORAN__RELATIONS_USER_PEG` FOREIGN KEY (`ID_PEGAWAI`) REFERENCES `user_pegawai` (`ID_PEGAWAI`),
  ADD CONSTRAINT `laporan_harian_ibfk_1` FOREIGN KEY (`ID_MAKANAN`) REFERENCES `jenis_makanan` (`ID_MAKANAN`),
  ADD CONSTRAINT `laporan_harian_ibfk_2` FOREIGN KEY (`ID_TRANSAKSI`) REFERENCES `transaksi` (`ID_TRANSAKSI`);

--
-- Ketidakleluasaan untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD CONSTRAINT `pembayaran_ibfk_1` FOREIGN KEY (`ID_TRANSAKSI`) REFERENCES `transaksi` (`ID_TRANSAKSI`);

--
-- Ketidakleluasaan untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`ID_INVEST`) REFERENCES `jenis_invest` (`ID_INVEST`),
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`ID_MEMBER`) REFERENCES `user_member` (`ID_MEMBER`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
